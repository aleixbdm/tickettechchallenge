# TechChallenge

Title: LiveStyled Android coding challange

Objective: Display a list of the next 50 events in London from the ticketmaster API.

Specifications:

- Developed in Kotlin and AndroidX with its last versions.

- RXJava to manage multi-threading.

- GSON and Retrofit for web service.

- Room for database management.

- ViewModel and LiveData to view communication and changes.

Followed steps:

1) Parse data

First of all, copy the JSON files with sample data in the test directory to be able to read them from unit tests.

The JSON is received in the web service communication channel. All the implementation will be organized in this layer.

For each type of response there's created a POJO class.

GSON library is used for (de)serialization.

The firsts tests implemented are only based on reading the JSON files and checking that the responses are created correctly.

Secondly, define the REST API interfaces to connect the web service and client. For now, they are simple without any parameter.

The Event class is used to manage success and handle error responses.

As retrofit is used to create the APIs, this library is tested using the MockWebServer implementation.

Finally, the tests check the web service implementation for any type of response received.

2) Save data

It has been decided to save the event list on the local database to improve efficiency consulting the main view of the application.

The instrumentation tests implemented to create a clean database in memory and load the JSON into the database.

3) Manage data

The Repository has the responsibility of managing the downloaded data and pass to the View Model which will provide to the View.

In the case of Event management, they are downloaded from the server and then saved to the database.

Once stored the LiveData component triggers an event returning the entities list.

The tests use Mockito to fake web services and DAOs.

4) View Model

The next step before working with UI is creating the ViewModels.

They have a dependency on the Repository and uses its data to parse for the view.

Also, provide feedback about the current process of the requests.

5) Show Data

The last step, show the data reactively.

Events are shown in a list ordered by date.

For each event, is shown all the information demanded using CardView.

The architecture of the Application is based on one single activity and fragments for each screen.

For that reason, there are single fragment tests and activity tests.

On one hand, the fragment tests are very similar to unit tests because they are mocking the view model connection.

On the other hand, the activity test is working with real dependencies where it could be a good opportunity to capture app screenshots.

6) Accomplishments

The result is an application being able to download events from the network and show it to the user responsively.

If the user has no internet connection, the events downloaded before will be still available to consult.

The project has a high code coverage regarding the layers tested in the unit tests.

Besides, there are instrumentation tests for every activity/fragment and the database.

7) Improvements

- Handle internet connection errors and reconnection to refresh data.
- Add more functionality to the event list: pull to refresh, filter, sort.
- Manage view model status when executing actions. (For instance, refresh the list)
- Add code coverage to instrumentation tests.
- Fake web service layer.