package aleixbdm.com.techchallenge.ui.event

import aleixbdm.com.techchallenge.R
import aleixbdm.com.techchallenge.app.AndroidTest
import aleixbdm.com.techchallenge.app.ManualAppComponent
import aleixbdm.com.techchallenge.app.TestViewModelFactory
import aleixbdm.com.techchallenge.database.event.toEntity
import aleixbdm.com.techchallenge.matchers.RecyclerViewMatchers.hasItemCount
import aleixbdm.com.techchallenge.readJson
import aleixbdm.com.techchallenge.repository.event.toModel
import aleixbdm.com.techchallenge.testContext
import aleixbdm.com.techchallenge.viewmodel.ManualViewModelResolverModule
import aleixbdm.com.techchallenge.viewmodel.event.EventViewData
import aleixbdm.com.techchallenge.viewmodel.event.EventViewModel
import aleixbdm.com.techchallenge.viewmodel.event.toViewData
import aleixbdm.com.techchallenge.webservice.WebServiceInjector
import aleixbdm.com.techchallenge.webservice.event.EventListResponse
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Test
import org.junit.runner.RunWith
import shared.ResultTestData
import shared.fromJson

@RunWith(AndroidJUnit4::class)
class EventFragmentTest : AndroidTest() {

    private val converter = WebServiceInjector.converter()
    private val viewModel: EventViewModel = mock()
    override val component = ManualAppComponent.inject(
        ManualViewModelResolverModule.viewModelResolver(
            viewModelsMap = mapOf(
                EventViewModel::class.java to lazy { TestViewModelFactory(viewModel) }
            )
        )
    )

    /** Event List LiveData **/

    @Test
    fun eventListLiveData() {
        val context = testContext()
        val json = context.readJson("eventList.json")
        val response: EventListResponse = converter.fromJson(json)
        val viewData = response.embedded.eventList
            .map { it.toEntity() }.map { it.toModel() }.map { it.toViewData() }
        val data = ResultTestData.success(viewData)

        eventListLiveDataTest(
            data = data
        )
    }

    @Test
    fun eventListLiveData_when_error() {
        eventListLiveDataTest(
            data = ResultTestData.error()
        )
    }

    private fun eventListLiveDataTest(data: ResultTestData<List<EventViewData>>) {
        whenever(viewModel.eventListLiveData).doAnswer {
            data.toLiveData()
        }

        launchFragmentInContainer<EventFragment>()

        verify(viewModel).eventListLiveData

        data.result.evaluate(onSuccess = {
            onView(withId(R.id.eventRecyclerView)).check(matches(hasItemCount(it.size)))
        }, onError = {
            onView(withId(R.id.eventRecyclerView)).check(matches(hasItemCount(0)))
        })
    }
}