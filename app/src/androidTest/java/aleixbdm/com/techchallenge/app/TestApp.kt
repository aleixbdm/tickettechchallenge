package aleixbdm.com.techchallenge.app

class TestApp : App() {

    lateinit var testComponent: AppComponent

    override val component: AppComponent
        get() = testComponent
}