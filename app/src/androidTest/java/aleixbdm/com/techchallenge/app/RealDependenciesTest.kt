package aleixbdm.com.techchallenge.app

import aleixbdm.com.techchallenge.testContext

abstract class RealDependenciesTest : AndroidTest() {

    override val component: AppComponent
        get() {
            val module = AppModuleDefinition(testContext())
            return AppInjector.manualComponent(module)
        }
}