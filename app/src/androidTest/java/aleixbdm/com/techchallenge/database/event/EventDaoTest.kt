package aleixbdm.com.techchallenge.database.event

import aleixbdm.com.techchallenge.database.AppDatabase
import aleixbdm.com.techchallenge.database.DatabaseTest
import aleixbdm.com.techchallenge.mutableMap
import aleixbdm.com.techchallenge.readJson
import aleixbdm.com.techchallenge.testContext
import aleixbdm.com.techchallenge.waitForLiveDataValue
import aleixbdm.com.techchallenge.webservice.WebServiceInjector
import aleixbdm.com.techchallenge.webservice.event.EventListResponse
import aleixbdm.com.techchallenge.webservice.event.EventResponse
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import shared.TestModule
import shared.fromJson

@RunWith(AndroidJUnit4::class)
class EventDaoTest : DatabaseTest<EventDao>(Module) {

    private val converter = WebServiceInjector.converter()

    private object Module : EventDaoModule,
        TestModule<EventDao, AppDatabase> {
        override fun resolve(resolver: AppDatabase) =
            dao(resolver)
    }

    /** Get & Insert Event List **/

    @Test
    fun initial_status() {
        val actual = waitForLiveDataValue(
            testable.getEventList()
        )
        assertEquals(0, actual.size)
    }

    @Test
    fun getEventList_and_insertEventList() {
        val context = testContext()
        val json = context.readJson("eventList.json")
        val response: EventListResponse = converter.fromJson(json)
        val listEntity = response.embedded.eventList.map { it.toEntity() }
        testable.insertEventWithImagesList(listEntity)

        val expected = listEntity.toMutableList()
        var imageId = 0
        expected.mutableMap { entity ->
            val modifiedEntity = EventWithImagesEntity()
            val imageList = entity.imageList.toMutableList().apply {
                mutableMap {
                    EventImageEntity(
                        id = ++imageId,
                        ratio = it.ratio,
                        url = it.url,
                        eventId = it.eventId
                    )
                }
            }
            modifiedEntity.event = entity.event
            modifiedEntity.imageList = imageList
            modifiedEntity
        }

        val actual = waitForLiveDataValue(
            testable.getEventList()
        )
        assertEquals(expected.size, actual.size)
        val expectedEvents = expected.map { it.event }
        val actualEvents = actual.map { it.event }
        assertTrue(expectedEvents.containsAll(actualEvents))
        val expectedImageList = expected.map { it.imageList }
        val actualImageList = expected.map { it.imageList }
        assertTrue(expectedImageList.containsAll(actualImageList))
    }

    /** Insert & Remove Favourite **/

    @Test
    fun insert_favourite() {
        val context = testContext()
        val json = context.readJson("event.json")
        val response: EventResponse = converter.fromJson(json)
        val eventEntity = response.toEntity()

        val favouriteEntity = FavouriteEntity(eventId = eventEntity.event.id)
        testable.insertEventWithImagesList(listOf(eventEntity))
        testable.insertFavourite(favouriteEntity)
        val actual = waitForLiveDataValue(
            testable.getEventList()
        )
        assertEquals(1, actual.size)
        assertEquals(favouriteEntity, actual.first().favouriteEntity.first())
    }

    @Test
    fun remove_favourite() {
        val context = testContext()
        val json = context.readJson("event.json")
        val response: EventResponse = converter.fromJson(json)
        val eventEntity = response.toEntity()

        val favouriteEntity = FavouriteEntity(eventId = eventEntity.event.id)

        insert_favourite()

        testable.removeFavourite(favouriteEntity)
        val actual = waitForLiveDataValue(
            testable.getEventList()
        )
        assertEquals(1, actual.size)
        assertEquals(0, actual.first().favouriteEntity.size)
    }

    @Test
    fun insert_twice_does_not_replace_favourite() {
        val context = testContext()
        val json = context.readJson("event.json")
        val response: EventResponse = converter.fromJson(json)
        val eventEntity = response.toEntity()

        val favouriteEntity = FavouriteEntity(eventId = eventEntity.event.id)

        insert_favourite()

        testable.insertEventWithImagesList(listOf(eventEntity))
        val actual = waitForLiveDataValue(
            testable.getEventList()
        )
        assertEquals(1, actual.size)
        assertEquals(favouriteEntity, actual.first().favouriteEntity.first())
    }

    /** Get Favourite List **/

    @Test
    fun getFavouriteList() {
        val context = testContext()
        val json = context.readJson("eventList.json")
        val response: EventListResponse = converter.fromJson(json)
        val listEntity = response.embedded.eventList.map { it.toEntity() }
        testable.insertEventWithImagesList(listEntity)

        val eventEntity = listEntity.first()
        val favouriteEntity = FavouriteEntity(eventId = eventEntity.event.id)
        testable.insertFavourite(favouriteEntity)
        val actual = waitForLiveDataValue(
            testable.getFavouriteList()
        )
        assertEquals(1, actual.size)
        assertEquals(favouriteEntity, actual.first().favouriteEntity.first())
    }
}