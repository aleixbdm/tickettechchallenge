package aleixbdm.com.techchallenge.database

import aleixbdm.com.techchallenge.testContext
import android.content.Context
import androidx.room.Room
import org.junit.After
import org.junit.Before
import org.junit.Rule
import shared.TestModule
import shared.liveDataRule

abstract class DatabaseTest<Testable : Any>(private val module: TestModule<Testable, AppDatabase>) {

    private object Module : DatabaseModule {
        override fun database(applicationContext: Context) =
            Room.inMemoryDatabaseBuilder(
                applicationContext, AppDatabase::class.java
            ).build()
    }

    @get:Rule
    val liveDataRule = liveDataRule()

    private lateinit var database: AppDatabase
    lateinit var testable: Testable

    @Before
    fun before() {
        val context = testContext()
        database = Module.database(context)
        testable = module.resolve(database)
    }

    @After
    fun after() {
        database.close()
    }
}