package aleixbdm.com.techchallenge.database.event

import aleixbdm.com.techchallenge.database.AppDatabase
import androidx.lifecycle.LiveData
import androidx.room.*

/** Dao **/

@Dao
interface EventDao {
    @Transaction
    @Query("SELECT * FROM event ORDER BY dateTime ASC")
    fun getEventList(): LiveData<List<EventWithImagesEntity>>

    @Transaction
    @Query("SELECT * FROM event, favourite WHERE event.id == favourite.eventId ORDER BY dateTime ASC")
    fun getFavouriteList(): LiveData<List<EventWithImagesEntity>>

    @Transaction
    fun insertEventWithImagesList(eventList: List<EventWithImagesEntity>) {
        insertEventList(eventList.map { it.event })
        insertEventImageList(eventList.map { it.imageList }.flatten())
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEventList(eventList: List<EventEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEventImageList(eventList: List<EventImageEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavourite(favourite: FavouriteEntity)

    @Delete
    fun removeFavourite(favourite: FavouriteEntity)
}

/** Module **/

interface EventDaoModule {
    fun dao(database: AppDatabase) =
        database.eventDao()
}