package aleixbdm.com.techchallenge.database.event

import androidx.room.Entity
import androidx.room.PrimaryKey

/** Entity **/

@Entity(tableName = "event_image")
data class EventImageEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val ratio: String,
    val url: String,
    val eventId: String
)