package aleixbdm.com.techchallenge.database.event

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey

/** Entity **/

@Entity(tableName = "favourite")
data class FavouriteEntity(
    @PrimaryKey
    val eventId: String
)