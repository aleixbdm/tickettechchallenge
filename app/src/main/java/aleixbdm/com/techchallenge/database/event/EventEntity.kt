package aleixbdm.com.techchallenge.database.event

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

/** Entity **/

@Entity(tableName = "event")
data class EventEntity(
    @PrimaryKey
    val id: String,
    val name: String,
    @Embedded
    val venue: EventVenueEntity,
    @Embedded
    val date: EventDateEntity?
)

data class EventVenueEntity(
    @ColumnInfo(name = "venue_name")
    val name: String
)
data class EventDateEntity(
    val dateTime: Long
)