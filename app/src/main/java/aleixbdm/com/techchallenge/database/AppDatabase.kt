package aleixbdm.com.techchallenge.database

import aleixbdm.com.techchallenge.database.event.*
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

/** Database **/

@Database(entities = [EventEntity::class, EventImageEntity::class, FavouriteEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun eventDao(): EventDao
}

/** Module **/

private const val DATABASE_NAME = "tech_challenge_db"

interface DatabaseModule {
    fun database(applicationContext: Context): AppDatabase =
        Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java,
            DATABASE_NAME
        )
            .fallbackToDestructiveMigration()
            .build()
}