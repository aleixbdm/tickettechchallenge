package aleixbdm.com.techchallenge.database.event

import aleixbdm.com.techchallenge.webservice.event.EventDateResponse
import aleixbdm.com.techchallenge.webservice.event.EventImageResponse
import aleixbdm.com.techchallenge.webservice.event.EventResponse
import aleixbdm.com.techchallenge.webservice.event.EventVenueResponse
import androidx.room.*

/** Entity **/

class EventWithImagesEntity {
    @Embedded
    lateinit var event: EventEntity
    @Relation(parentColumn = "id", entityColumn = "eventId")
    lateinit var imageList: List<EventImageEntity>
    @Relation(parentColumn = "id", entityColumn = "eventId")
    var favouriteEntity: List<FavouriteEntity> = emptyList()
}

/** Conversion **/

fun EventResponse.toEntity() = EventWithImagesEntity().apply {
    event = toEventEntity()
    imageList = this@toEntity.imageList.map { it.toEntity(id) }
}

private fun EventResponse.toEventEntity() = EventEntity(
    id = id,
    name = name,
    venue = embedded.venueList.first().toEntity(),
    date = date.toEntity()
)

private fun EventVenueResponse.toEntity() = EventVenueEntity(
    name = name
)

private fun EventDateResponse.toEntity() = start.dateTime?.let {
    EventDateEntity(
        dateTime = it.time
    )
}

private fun EventImageResponse.toEntity(eventId: String) = EventImageEntity(
    ratio = ratio,
    url = url,
    eventId = eventId
)