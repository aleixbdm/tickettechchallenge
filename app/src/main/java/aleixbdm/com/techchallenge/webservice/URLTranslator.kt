package aleixbdm.com.techchallenge.webservice

const val URL_EventList = "events.json"
const val QUERY_EventList_ApiKey = "apikey"
const val QUERY_EventList_City = "city"
const val QUERY_EventList_Size = "size"