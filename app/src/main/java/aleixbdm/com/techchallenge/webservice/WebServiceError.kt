package aleixbdm.com.techchallenge.webservice

class WebServiceError(cause: Throwable) : Exception(cause.message, cause) {
    constructor(message: String?) : this(Throwable(message, null))
}