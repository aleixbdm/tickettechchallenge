package aleixbdm.com.techchallenge.webservice.event

import com.google.gson.annotations.SerializedName
import java.util.*

/** Event List **/

data class EventListResponse(
    @SerializedName("_embedded")
    val embedded: Embedded
) {
    data class Embedded(
        @SerializedName("events")
        val eventList: List<EventResponse>
    )
}

/** Event **/

data class EventResponse(
    val id: String,
    val name: String,
    @SerializedName("images")
    val imageList: List<EventImageResponse>,
    @SerializedName("_embedded")
    val embedded: Embedded,
    @SerializedName("dates")
    val date: EventDateResponse
) {
    data class Embedded(
        @SerializedName("venues")
        val venueList: List<EventVenueResponse>
    )
}

/** Image **/

data class EventImageResponse(
    val ratio: String,
    val url: String
)

/** Venue **/

data class EventVenueResponse(
    val name: String
)

/** Date **/

data class EventDateResponse(
    val start: StartDate
) {
    data class StartDate(
        val dateTime: Date?
    )
}