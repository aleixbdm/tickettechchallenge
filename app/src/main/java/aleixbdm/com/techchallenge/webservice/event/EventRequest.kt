package aleixbdm.com.techchallenge.webservice.event

/** Request **/

private const val EVENT_CITY = "London"
private const val EVENT_SIZE = 50

data class EventRequest(
    val apiKey: String,
    val city: String = EVENT_CITY,
    val size: Int = EVENT_SIZE
)