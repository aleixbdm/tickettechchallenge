package aleixbdm.com.techchallenge.webservice

import aleixbdm.com.techchallenge.core.Result
import io.reactivex.Single

fun <T> Single<T>.resolve() : Single<Result<T>> =
    map { Result.success(it) }
        .onErrorResumeNext { throwable: Throwable ->
            Single.just(Result.error(WebServiceError(throwable)))
        }