package aleixbdm.com.techchallenge.webservice.event

import aleixbdm.com.techchallenge.webservice.*
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/** Rest API
 *
 *  GET     /events.json
 */

/** Retrofit rest API **/

interface RetrofitEventRestAPI {

    @GET(URL_EventList)
    fun getEventList(
        @Query(QUERY_EventList_ApiKey) apiKey: String,
        @Query(QUERY_EventList_City) city: String,
        @Query(QUERY_EventList_Size) size: Int
    ): Single<EventListResponse>
}