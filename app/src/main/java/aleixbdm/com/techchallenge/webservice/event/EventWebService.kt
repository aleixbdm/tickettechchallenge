package aleixbdm.com.techchallenge.webservice.event

import aleixbdm.com.techchallenge.core.Result
import aleixbdm.com.techchallenge.webservice.resolve
import io.reactivex.Single
import retrofit2.Retrofit

/** Web service **/

interface EventWebService {
    fun getEventList(request: EventRequest): Single<Result<EventListResponse>>
}

/** Retrofit **/

private class RetrofitEventWebService(retrofit: Retrofit) : EventWebService {

    private val api = retrofit.create(RetrofitEventRestAPI::class.java)

    override fun getEventList(request: EventRequest) = request.run {
        api.getEventList(
            apiKey = apiKey,
            city = city,
            size = size
        ).resolve()
    }
}

/** Module **/

interface RetrofitEventWebServiceModule {
    fun webService(retrofit: Retrofit): EventWebService =
        RetrofitEventWebService(retrofit)
}