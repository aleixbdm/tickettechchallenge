package aleixbdm.com.techchallenge.app

import aleixbdm.com.techchallenge.core.rx.SchedulerProvider
import aleixbdm.com.techchallenge.database.AppDatabase
import aleixbdm.com.techchallenge.database.DatabaseModule
import aleixbdm.com.techchallenge.database.event.EventDao
import aleixbdm.com.techchallenge.database.event.EventDaoModule
import aleixbdm.com.techchallenge.repository.event.EventRepository
import aleixbdm.com.techchallenge.repository.event.EventRepositoryModule
import aleixbdm.com.techchallenge.viewmodel.ManualViewModelResolverModule
import aleixbdm.com.techchallenge.viewmodel.ViewModelResolver
import aleixbdm.com.techchallenge.viewmodel.event.EventViewModel
import aleixbdm.com.techchallenge.viewmodel.event.EventViewModelModule
import aleixbdm.com.techchallenge.viewmodel.event.EventViewModelResolver
import aleixbdm.com.techchallenge.webservice.event.EventWebService
import aleixbdm.com.techchallenge.webservice.event.RetrofitEventWebServiceModule
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import retrofit2.Retrofit

/** App Component **/

interface ManualAppComponent : AppComponent {
    companion object {
        fun inject(viewModelResolver: ViewModelResolver): AppComponent {
            return object : ManualAppComponent {
                override val viewModelResolver = viewModelResolver
            }
        }

        fun inject(appModule: AppModule) = {
            val applicationContext = appModule.applicationContext()
            val retrofit = appModule.retrofitModule().retrofit()
            val schedulerProvider = SchedulerProviderFactory.provider()
            val eventRepository = lazy {
                RepositoryFactory.eventRepository(
                    WebServiceFactory.eventWebService(retrofit),
                    DaoFactory.eventDao(
                        DatabaseFactory.database(applicationContext)
                    ),
                    schedulerProvider
                )
            }
            val viewModelResolver = ViewModelFactory.viewModelResolver(
                eventRepository,
                schedulerProvider
            )
            inject(viewModelResolver)
        }()
    }

    val viewModelResolver: ViewModelResolver

    override fun inject(fragment: InjectableFragment) {
        fragment.viewModelResolver = viewModelResolver
    }
}

/** Schedule Provider **/

private object SchedulerProviderFactory {
    fun provider() =
        object : SchedulerProvider {}
}

/** Web service **/

private object WebServiceFactory {
    private val eventtModule by lazy { object : RetrofitEventWebServiceModule {} }

    fun eventWebService(retrofit: Retrofit) =
        eventtModule.webService(retrofit)
}

/** Database **/

private object DatabaseFactory {
    private val module by lazy { object : DatabaseModule {} }
    fun database(applicationContext: Context) =
        module.database(applicationContext)
}

/** Dao **/

private object DaoFactory {
    private val eventModule by lazy { object : EventDaoModule {} }
    fun eventDao(database: AppDatabase) =
        eventModule.dao(database)
}

/** Repository **/

private object RepositoryFactory {
    private val eventModule by lazy { object : EventRepositoryModule {} }

    fun eventRepository(
        webService: EventWebService,
        dao: EventDao,
        schedulerProvider: SchedulerProvider
    ) =
        eventModule.repository(webService, dao, schedulerProvider)
}

/** ViewModel **/

private object ViewModelFactory {
    fun viewModelResolver(
        eventRepository: Lazy<EventRepository>,
        schedulerProvider: SchedulerProvider
    ) = {
        val viewModelsMap: Map<Class<out ViewModel>, Lazy<ViewModelProvider.Factory>> = mapOf(
            EventViewModel::class.java to
                    lazy {
                        val repository by eventRepository
                        val resolver = EventViewModelResolver(
                            repository,
                            schedulerProvider
                        )
                        EventViewModelModule.factory(resolver)
                    }
        )
        ManualViewModelResolverModule.viewModelResolver(viewModelsMap)
    }()
}