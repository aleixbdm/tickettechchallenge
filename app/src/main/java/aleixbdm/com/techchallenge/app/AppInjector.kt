package aleixbdm.com.techchallenge.app

object AppInjector {

    fun manualComponent(appModule: AppModule): AppComponent =
        ManualAppComponent.inject(appModule)
}