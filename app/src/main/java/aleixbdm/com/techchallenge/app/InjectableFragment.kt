package aleixbdm.com.techchallenge.app

import aleixbdm.com.techchallenge.viewmodel.ViewModelResolver
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel

abstract class InjectableFragment : Fragment() {
    lateinit var viewModelResolver: ViewModelResolver

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity?.application as? App)?.component?.inject(this)
    }
}

inline fun <reified T : ViewModel> InjectableFragment.resolveViewModel() = lazy {
    viewModelResolver.resolve(this, T::class.java)
}