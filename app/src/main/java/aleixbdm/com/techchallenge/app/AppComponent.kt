package aleixbdm.com.techchallenge.app

import aleixbdm.com.techchallenge.webservice.WebServiceInjector
import android.content.Context

/** Component **/

interface AppComponent {
    fun inject(fragment: InjectableFragment)
}

/** Module **/

interface AppModule {
    fun applicationContext(): Context
    fun retrofitModule(): WebServiceInjector.Module
}