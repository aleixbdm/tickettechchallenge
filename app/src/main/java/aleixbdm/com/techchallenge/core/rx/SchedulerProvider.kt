package aleixbdm.com.techchallenge.core.rx

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface SchedulerProvider {
    fun main(): Scheduler =
        AndroidSchedulers.mainThread()

    fun io(): Scheduler =
        Schedulers.io()
}