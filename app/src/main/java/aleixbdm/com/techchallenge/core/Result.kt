package aleixbdm.com.techchallenge.core

sealed class Result<out T> {
    private data class Success<out T>(val data: T) : Result<T>()
    private data class Error(val exception: Exception) : Result<Nothing>() {
        override fun hashCode(): Int {
            return javaClass.hashCode()
        }

        override fun equals(other: Any?): Boolean {
            return if (other is Error) {
                other.exception.javaClass == exception.javaClass
            } else {
                return super.equals(other)
            }
        }
    }

    companion object {
        fun <T> success(data: T): Result<T> =
            Success(data)


        fun <T> error(exception: Exception): Result<T> =
            Error(exception)
    }

    fun tryGetValue(): T =
        when (this) {
            is Success -> data
            is Error -> throw exception
        }

    fun evaluate(
        onSuccess: (data: T) -> Unit,
        onError: (exception: Exception) -> Unit
    ) {
        when (this) {
            is Success -> onSuccess(data)
            is Error -> onError(exception)
        }
    }
}