package aleixbdm.com.techchallenge.viewmodel.event

interface FavouriteCallback {
    fun insertFavourite(eventId: String)
    fun removeFavourite(eventId: String)
}