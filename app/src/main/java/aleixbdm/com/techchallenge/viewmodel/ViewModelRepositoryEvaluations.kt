package aleixbdm.com.techchallenge.viewmodel

import aleixbdm.com.techchallenge.core.Result
import aleixbdm.com.techchallenge.repository.RepositoryStatus
import aleixbdm.com.techchallenge.core.rx.SchedulerProvider
import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver

fun evaluateResult(
    schedulerProvider: SchedulerProvider,
    result: Single<Result<RepositoryStatus>>,
    onCompletedCall: () -> Unit,
    onErrorCall: (Exception) -> Unit
) =
    result.subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.main())
        .subscribeWith(object : DisposableSingleObserver<Result<RepositoryStatus>>() {
            override fun onSuccess(t: Result<RepositoryStatus>) {
                t.evaluate(onSuccess = {
                    when (it) {
                        RepositoryStatus.Completed -> onCompletedCall()
                    }
                }, onError = onErrorCall)
            }

            override fun onError(e: Throwable) {
                onErrorCall(Exception(e))
            }
        })