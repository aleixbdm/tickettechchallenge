package aleixbdm.com.techchallenge.viewmodel.event

import aleixbdm.com.techchallenge.BuildConfig
import aleixbdm.com.techchallenge.core.Result
import aleixbdm.com.techchallenge.core.rx.SchedulerProvider
import aleixbdm.com.techchallenge.repository.event.EventRepository
import aleixbdm.com.techchallenge.repository.kotlinTransformationsMapList
import aleixbdm.com.techchallenge.viewmodel.ViewModelModule
import aleixbdm.com.techchallenge.viewmodel.ViewModelStatus
import aleixbdm.com.techchallenge.viewmodel.evaluateResult
import aleixbdm.com.techchallenge.webservice.event.EventRequest
import androidx.lifecycle.*
import io.reactivex.disposables.CompositeDisposable


/** View Model **/

abstract class EventViewModel : ViewModel(), LifecycleObserver,
    FavouriteCallback {
    abstract val getEventListStatus: LiveData<Result<ViewModelStatus>>
    abstract val insertFavouriteStatus: LiveData<Result<ViewModelStatus>>
    abstract val removeFavouriteStatus: LiveData<Result<ViewModelStatus>>
    abstract val eventListLiveData: LiveData<List<EventViewData>>
    abstract val favouriteListLiveData: LiveData<List<EventViewData>>
    abstract fun getEventList()
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        getEventList()
    }
}

/** Definition **/

private class EventViewModelDefinition(
    private val repository: EventRepository,
    private val schedulerProvider: SchedulerProvider
) : EventViewModel() {

    private var disposable: CompositeDisposable?
    private val _getEventListStatus = MutableLiveData<Result<ViewModelStatus>>()
    override val getEventListStatus: LiveData<Result<ViewModelStatus>>
        get() = _getEventListStatus
    private val _insertFavouriteStatus = MutableLiveData<Result<ViewModelStatus>>()
    override val insertFavouriteStatus: LiveData<Result<ViewModelStatus>>
        get() = _insertFavouriteStatus
    private val _removeFavouriteStatus = MutableLiveData<Result<ViewModelStatus>>()
    override val removeFavouriteStatus: LiveData<Result<ViewModelStatus>>
        get() = _removeFavouriteStatus

    init {
        disposable = CompositeDisposable()
    }

    override fun onCleared() {
        super.onCleared()
        if (disposable != null) {
            disposable?.clear()
            disposable = null
        }
    }

    override val eventListLiveData by lazy(LazyThreadSafetyMode.NONE) {
        kotlinTransformationsMapList(repository.eventListLiveData) { list ->
            list.map { it.toViewData() }
        }
    }

    override val favouriteListLiveData by lazy(LazyThreadSafetyMode.NONE) {
        kotlinTransformationsMapList(repository.favouriteListLiveData) { list ->
            list.map { it.toViewData() }
        }
    }

    override fun getEventList() {
        val request = EventRequest(apiKey = BuildConfig.TicketMasterAPIKey)
        _getEventListStatus.value = Result.success(ViewModelStatus.Loading)
        val onCompleted = {
            _getEventListStatus.value = Result.success(ViewModelStatus.Completed)
        }
        val onError = { exception: Exception ->
            _getEventListStatus.value = Result.error(exception)
        }
        disposable?.add(
            evaluateResult(
                schedulerProvider = schedulerProvider,
                result = repository.getEventList(request),
                onCompletedCall = onCompleted,
                onErrorCall = onError
            )
        )
    }

    override fun insertFavourite(eventId: String) {
        _insertFavouriteStatus.value = Result.success(ViewModelStatus.Loading)
        val onCompleted = {
            _insertFavouriteStatus.value = Result.success(ViewModelStatus.Completed)
        }
        val onError = { exception: Exception ->
            _insertFavouriteStatus.value = Result.error(exception)
        }
        disposable?.add(
            evaluateResult(
                schedulerProvider = schedulerProvider,
                result = repository.insertFavourite(eventId),
                onCompletedCall = onCompleted,
                onErrorCall = onError
            )
        )
    }

    override fun removeFavourite(eventId: String) {
        _removeFavouriteStatus.value = Result.success(ViewModelStatus.Loading)
        val onCompleted = {
            _removeFavouriteStatus.value = Result.success(ViewModelStatus.Completed)
        }
        val onError = { exception: Exception ->
            _removeFavouriteStatus.value = Result.error(exception)
        }
        disposable?.add(
            evaluateResult(
                schedulerProvider = schedulerProvider,
                result = repository.removeFavourite(eventId),
                onCompletedCall = onCompleted,
                onErrorCall = onError
            )
        )
    }
}

/** Module **/

data class EventViewModelResolver(
    val repository: EventRepository,
    val schedulerProvider: SchedulerProvider
)

object EventViewModelModule : ViewModelModule<EventViewModel, EventViewModelResolver>(
    viewModelResolution = {
        EventViewModelDefinition(it.repository, it.schedulerProvider)
    }
)