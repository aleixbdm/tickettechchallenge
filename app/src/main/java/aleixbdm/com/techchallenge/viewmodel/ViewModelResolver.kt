package aleixbdm.com.techchallenge.viewmodel

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

/** View Model Resolver **/

interface ViewModelResolver {
    fun <T : ViewModel> resolve(fragment: Fragment, viewModelClass: Class<T>): T
}

/** Manual **/

private class ManualViewModelResolver(
    private val viewModelsMap: Map<Class<out ViewModel>, Lazy<ViewModelProvider.Factory>>
) : ViewModelResolver {

    override fun <T : ViewModel> resolve(fragment: Fragment, viewModelClass: Class<T>): T {
        val factory by viewModelsMap[viewModelClass] ?: viewModelsMap.asIterable().firstOrNull {
            viewModelClass.isAssignableFrom(it.key)
        }?.value ?: throw IllegalArgumentException("Unknown model class $viewModelClass")
        return fragment.viewModel(factory, viewModelClass)
    }
}

/** Module **/

object ManualViewModelResolverModule {
    fun viewModelResolver(
        viewModelsMap: Map<Class<out ViewModel>, Lazy<ViewModelProvider.Factory>>
    ): ViewModelResolver =
        ManualViewModelResolver(viewModelsMap)
}

/** Fragment view model providers **/

private fun <T : ViewModel> Fragment.viewModel(
    factory: ViewModelProvider.Factory,
    viewModelClass: Class<T>
) =
    activity?.let {
        ViewModelProviders
            .of(it, factory)
            .get(viewModelClass)
    } ?: run {
        ViewModelProviders
            .of(this, factory)
            .get(viewModelClass)
    }