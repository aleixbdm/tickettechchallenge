package aleixbdm.com.techchallenge.viewmodel.event

import aleixbdm.com.techchallenge.repository.event.EventImageModel
import aleixbdm.com.techchallenge.repository.event.EventModel
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

/** View Data **/

data class EventViewData(
    val id: String,
    val name: String,
    val venueName: String,
    val dateTime: String?,
    val imageList: List<EventImageData>,
    val isFavourite: Boolean
)

typealias EventImageData = EventImageModel

/** Conversion **/

fun EventModel.toViewData() = {
    val dateTime = date?.dateTime?.let {
        val dateTimeFormat = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
        val localDate = LocalDateTime.ofInstant(it.toInstant(), ZoneId.systemDefault())
        localDate.format(dateTimeFormat)
    }
    EventViewData(
        id = id,
        name = name,
        venueName = venue.name,
        dateTime = dateTime,
        imageList = imageList,
        isFavourite = isFavourite
    )
}()