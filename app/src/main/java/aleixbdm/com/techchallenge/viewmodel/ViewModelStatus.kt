package aleixbdm.com.techchallenge.viewmodel

enum class ViewModelStatus {
    Loading, Completed
}