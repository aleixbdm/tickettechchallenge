package aleixbdm.com.techchallenge.repository.event

import aleixbdm.com.techchallenge.core.Result
import aleixbdm.com.techchallenge.database.event.EventDao
import aleixbdm.com.techchallenge.database.event.toEntity
import aleixbdm.com.techchallenge.repository.RepositoryStatus
import aleixbdm.com.techchallenge.core.rx.SchedulerProvider
import aleixbdm.com.techchallenge.database.event.FavouriteEntity
import aleixbdm.com.techchallenge.repository.kotlinTransformationsMapList
import aleixbdm.com.techchallenge.webservice.event.EventRequest
import aleixbdm.com.techchallenge.webservice.event.EventWebService
import androidx.lifecycle.LiveData
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.Single
import io.reactivex.disposables.Disposable

/** Repository **/

interface EventRepository {
    val eventListLiveData: LiveData<List<EventModel>>
    val favouriteListLiveData: LiveData<List<EventModel>>
    fun getEventList(request: EventRequest): Single<Result<RepositoryStatus>>
    fun insertFavourite(eventId: String): Single<Result<RepositoryStatus>>
    fun removeFavourite(eventId: String): Single<Result<RepositoryStatus>>
}

/** Definition **/

private class EventRepositoryDefinition(
    private val webService: EventWebService,
    private val dao: EventDao,
    private val schedulerProvider: SchedulerProvider
) : EventRepository {

    override val eventListLiveData by lazy(LazyThreadSafetyMode.NONE) {
        val liveData = dao.getEventList()
        kotlinTransformationsMapList(liveData) { list ->
            list.map { it.toModel() }
        }
    }

    override val favouriteListLiveData by lazy(LazyThreadSafetyMode.NONE) {
        val liveData = dao.getFavouriteList()
        kotlinTransformationsMapList(liveData) { list ->
            list.map { it.toModel() }
        }
    }

    override fun getEventList(request: EventRequest): Single<Result<RepositoryStatus>> =
        webService.getEventList(request)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.io())
            .flatMap { event ->
                try {
                    val response = event.tryGetValue()
                    val eventList = response.embedded.eventList.map { it.toEntity() }
                    dao.insertEventWithImagesList(eventList)
                    Single.just(Result.success(RepositoryStatus.Completed))
                } catch (exception: Exception) {
                    Single.just(Result.error<RepositoryStatus>(exception))
                }
            }

    override fun insertFavourite(eventId: String): Single<Result<RepositoryStatus>> =
        Completable.fromAction {
            val entity = FavouriteEntity(eventId)
            dao.insertFavourite(entity)
        }.observeOn(schedulerProvider.main())
            .subscribeOn(schedulerProvider.io()).toSingle {
                Result.success(RepositoryStatus.Completed)
            }.onErrorResumeNext {
                Single.just(Result.error(Exception(it)))
            }

    override fun removeFavourite(eventId: String): Single<Result<RepositoryStatus>> =
        Completable.fromAction {
            val entity = FavouriteEntity(eventId)
            dao.removeFavourite(entity)
        }.observeOn(schedulerProvider.main())
            .subscribeOn(schedulerProvider.io()).toSingle {
                Result.success(RepositoryStatus.Completed)
            }.onErrorResumeNext {
                Single.just(Result.error(Exception(it)))
            }
}

/** Module **/

interface EventRepositoryModule {
    fun repository(
        webService: EventWebService,
        dao: EventDao,
        schedulerProvider: SchedulerProvider
    ): EventRepository =
        EventRepositoryDefinition(webService, dao, schedulerProvider)
}