package aleixbdm.com.techchallenge.repository.event

import aleixbdm.com.techchallenge.database.event.EventDateEntity
import aleixbdm.com.techchallenge.database.event.EventImageEntity
import aleixbdm.com.techchallenge.database.event.EventVenueEntity
import aleixbdm.com.techchallenge.database.event.EventWithImagesEntity
import java.time.ZoneId
import java.util.*

/** Model **/

data class EventModel(
    val id: String,
    val name: String,
    val venue: EventVenueModel,
    val date: EventDateModel?,
    val imageList: List<EventImageModel>,
    val isFavourite: Boolean
)

data class EventVenueModel(
    val name: String
)

data class EventDateModel(
    val dateTime: Date
)

data class EventImageModel(
    val ratio: EventImageRatio,
    val url: String
)

enum class EventImageRatio(val value: String) {
    Horizontal("16_9"),
    Vertical32("3_2"),
    Vertical43("4_3");

    companion object {
        fun from(value: String) = values().first { it.value == value }
    }
}

/** Conversion **/

fun EventWithImagesEntity.toModel() = EventModel(
    id = event.id,
    name = event.name,
    venue = event.venue.toModel(),
    date = event.date?.toModel(),
    imageList = imageList.map { it.toModel() },
    isFavourite = !favouriteEntity.isEmpty()
)

private fun EventVenueEntity.toModel() = EventVenueModel(
    name = name
)

private fun EventDateEntity.toModel() = EventDateModel(
    dateTime = Date(dateTime)
)

private fun EventImageEntity.toModel() = EventImageModel(
    ratio = EventImageRatio.from(ratio),
    url = url
)