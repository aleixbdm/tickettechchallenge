package aleixbdm.com.techchallenge.ui.event

import aleixbdm.com.techchallenge.R
import aleixbdm.com.techchallenge.app.InjectableFragment
import aleixbdm.com.techchallenge.app.resolveViewModel
import aleixbdm.com.techchallenge.viewmodel.event.EventViewModel
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.event_fragment.*

class FavouriteFragment : InjectableFragment() {

    private val eventViewModel: EventViewModel by resolveViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.event_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = EventAdapter(eventViewModel)

        eventViewModel.favouriteListLiveData.observe(this, Observer {
            adapter.submitList(it)
        })

        eventRecyclerView.apply {
            this.adapter = adapter
        }
    }
}