package aleixbdm.com.techchallenge.ui.event

import aleixbdm.com.techchallenge.R
import aleixbdm.com.techchallenge.repository.event.EventImageRatio
import aleixbdm.com.techchallenge.viewmodel.event.EventViewData
import aleixbdm.com.techchallenge.viewmodel.event.FavouriteCallback
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class EventAdapter(private val callback: FavouriteCallback) :
    ListAdapter<EventViewData, EventAdapter.ViewHolder>(EventDiffCallback()) {

    class ViewHolder(val item: View) : RecyclerView.ViewHolder(item)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val eventViewData = getItem(position)
        val context = holder.item.context

        holder.item.findViewById<TextView>(R.id.eventItemNameTextView).text =
            eventViewData.name

        holder.item.findViewById<TextView>(R.id.eventItemVenueNameTextView).text =
            eventViewData.venueName

        holder.item.findViewById<TextView>(R.id.eventItemDateTextView).text =
            eventViewData.dateTime

        val url = eventViewData.imageList.first { it.ratio == EventImageRatio.Horizontal }.url
        val imageView = holder.item.findViewById<ImageView>(R.id.eventItemImageView)
        Glide.with(context)
            .load(url)
            .into(imageView)

        val favouriteButton = holder.item.findViewById<ImageButton>(R.id.eventItemFavouriteButton)
        eventViewData.run {
            val drawable = if (isFavourite) {
                R.drawable.ic_favourite_filled
            } else {
                R.drawable.ic_favourite_unfilled
            }
            favouriteButton.setImageResource(drawable)
        }

        favouriteButton.setOnClickListener {
            eventViewData.run {
                val drawable = if (isFavourite) {
                    callback.removeFavourite(id)
                    R.drawable.ic_favourite_unfilled
                } else {
                    callback.insertFavourite(id)
                    R.drawable.ic_favourite_filled
                }
                favouriteButton.setImageResource(drawable)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.event_view_item, parent, false)
        return ViewHolder(itemView)
    }
}

private class EventDiffCallback : DiffUtil.ItemCallback<EventViewData>() {

    override fun areItemsTheSame(oldItem: EventViewData, newItem: EventViewData): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: EventViewData, newItem: EventViewData): Boolean {
        return oldItem == newItem
    }
}