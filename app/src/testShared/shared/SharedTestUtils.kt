package shared

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.gson.Gson

inline fun <reified T> Gson.fromJson(json: String?): T =
    this.fromJson(json, T::class.java)

fun liveDataRule() =
    InstantTaskExecutorRule()

class FailTestException(reason: String) : Exception("Test failed: $reason")