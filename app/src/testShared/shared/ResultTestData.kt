package shared

import aleixbdm.com.techchallenge.core.Result
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/** Data **/

class ResultTestData<T> private constructor(
    val result: Result<T>
) {
    companion object {
        fun <T> success(data: T) = ResultTestData(
            Result.success(data)
        )

        fun <T> error() = ResultTestData(
            Result.error<T>(FailTestException("Error to test"))
        )
    }

    fun toLiveData(): LiveData<T> {
        val liveData = MutableLiveData<T>()
        try {
            val value = result.tryGetValue()
            liveData.value = value
        } catch (ex: Exception) {
        }
        return liveData
    }
}