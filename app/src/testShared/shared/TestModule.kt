package shared

interface TestModule<out Testable, in Resolver> {
    fun resolve(resolver: Resolver): Testable
}