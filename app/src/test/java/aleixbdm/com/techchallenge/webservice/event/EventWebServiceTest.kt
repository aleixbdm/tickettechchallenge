package aleixbdm.com.techchallenge.webservice.event

import aleixbdm.com.techchallenge.readJson
import aleixbdm.com.techchallenge.testEventRequest
import aleixbdm.com.techchallenge.webservice.*
import org.junit.Test
import retrofit2.Retrofit
import shared.TestModule

class EventWebServiceTest : WebServiceTest<EventWebService>(Module) {

    private object Module : RetrofitEventWebServiceModule,
        TestModule<EventWebService, Retrofit> {
        override fun resolve(resolver: Retrofit) =
            webService(resolver)
    }

    /** Get Event List **/

    @Test
    fun `get event list when success`() {
        val json = readJson("eventList.json")
        val data = WebServiceTestData.success(json)
        getEventListTest(data)
    }

    @Test
    fun `get event list when error`() {
        val data = WebServiceTestData.error()
        getEventListTest(data)
    }

    @Test
    fun `get event list when no data`() {
        val data = WebServiceTestData.noData()
        getEventListTest(data)
    }

    @Test
    fun `get event list when invalid data`() {
        val data = WebServiceTestData.invalid()
        getEventListTest(data)
    }

    @Test
    fun `get event list when empty`() {
        val data = WebServiceTestData.empty()
        getEventListTest(data)
    }

    private fun getEventListTest(data: WebServiceTestData) {
        val method = "GET"
        val path = URL_EventList
        val queryParameters = mapOf(
            QUERY_EventList_ApiKey to testEventRequest.apiKey,
            QUERY_EventList_City to testEventRequest.city,
            QUERY_EventList_Size to testEventRequest.size
        )
        val status = WebServiceTestStatus(
            path = path,
            method = method,
            queryParameters = queryParameters)
        test(
            data,
            { it.getEventList(testEventRequest) },
            status
        )
    }
}