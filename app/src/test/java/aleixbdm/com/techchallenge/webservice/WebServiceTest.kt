package aleixbdm.com.techchallenge.webservice

import aleixbdm.com.techchallenge.core.Result
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import retrofit2.Retrofit
import shared.TestModule
import io.reactivex.Single

abstract class WebServiceTest<Testable : Any>(private val module: TestModule<Testable, Retrofit>) {

    lateinit var testable: Testable
    val retrofitTester = {
        val mockWebServer = MockWebServer()
        RetrofitTesterModule.tester(mockWebServer)
    }()
    val retrofitChecker =
        RetrofitTestCheckerFactory.checker()

    @Before
    fun before() {
        val retrofit = retrofitTester.start()
        testable = module.resolve(retrofit)
    }

    @After
    fun after() {
        retrofitTester.stop()
    }

    inline fun <reified Response> test(
        data: WebServiceTestData,
        testBlock: (Testable) -> Single<Result<Response>>,
        status: WebServiceTestStatus
    ) {
        retrofitTester.enqueueRequest(data)

        val expected = data.toResult<Response>()
        val actual = testBlock.invoke(testable).test()

        actual.assertComplete()
        actual.assertValueCount(1)
        actual.assertValues(expected)
        actual.dispose()

        val execution = retrofitTester.takeRequest()
        retrofitChecker.check(
            listOf(execution),
            status
        )
    }
}