package aleixbdm.com.techchallenge.webservice.event

import aleixbdm.com.techchallenge.date
import aleixbdm.com.techchallenge.readJson
import aleixbdm.com.techchallenge.webservice.WebServiceInjector
import org.junit.Assert.assertEquals
import org.junit.Test
import shared.fromJson

class EventResponseTest {

    private val converter = WebServiceInjector.converter()

    @Test
    fun `parse event image json`() {
        val json = readJson("eventImage.json")
        val actual: EventImageResponse = converter.fromJson(json)
        val expected = EventImageResponse(
            ratio = "16_9",
            url = "https://s1.ticketm.net/dam/a/45b/2d7637a6-2686-4fe9-8563-175f0757a45b_1106721_RETINA_PORTRAIT_16_9.jpg"
        )

        assertEquals(expected, actual)
    }

    @Test
    fun `parse event venue json`() {
        val json = readJson("eventVenue.json")
        val actual: EventVenueResponse = converter.fromJson(json)
        val expected = EventVenueResponse(
            name = "Tottenham Hotspur Stadium"
        )

        assertEquals(expected, actual)
    }

    @Test
    fun `parse event date json`() {
        val json = readJson("eventDate.json")
        val actual: EventDateResponse = converter.fromJson(json)
        val expected = EventDateResponse(
            start = EventDateResponse.StartDate(
                dateTime = date("2019-10-13T13:30:00Z")
            )
        )

        assertEquals(expected, actual)
    }

    @Test
    fun `parse event json`() {
        val venue: EventVenueResponse = readJson("eventVenue.json")
            .let { converter.fromJson(it) }
        val date: EventDateResponse = readJson("eventDate.json")
            .let { converter.fromJson(it) }

        val json = readJson("event.json")
        val actual: EventResponse = converter.fromJson(json)
        val expected = EventResponse(
            id = "17u8vpG6u5rm_rb",
            name = "NFL: Carolina Panthers v Tampa Bay Buccaneers",
            imageList = actual.imageList,
            embedded = EventResponse.Embedded(
                venueList = listOf(venue)
            ),
            date = date
        )

        assertEquals(expected, actual)
    }

    @Test
    fun `parse event list json`() {
        val json = readJson("eventList.json")
        val actual: EventListResponse = converter.fromJson(json)
        val expected = 50
        assertEquals(expected, actual.embedded.eventList.size)
    }
}