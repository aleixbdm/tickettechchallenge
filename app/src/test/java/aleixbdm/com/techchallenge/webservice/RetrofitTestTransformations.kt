package aleixbdm.com.techchallenge.webservice

import aleixbdm.com.techchallenge.core.Result
import retrofit2.Response

fun <T> Response<T>.toResult(): Result<T> {
    return body()?.let {
        Result.success(it)
    } ?: run {
        Result.error<T>(WebServiceError(errorBody()?.string()))
    }
}