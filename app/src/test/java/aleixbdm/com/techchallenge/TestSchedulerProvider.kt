package aleixbdm.com.techchallenge

import aleixbdm.com.techchallenge.core.rx.SchedulerProvider
import io.reactivex.Scheduler
import io.reactivex.internal.schedulers.TrampolineScheduler

object TestSchedulerProvider: SchedulerProvider {
    override fun main(): Scheduler =
        TrampolineScheduler.instance()

    override fun io(): Scheduler =
        TrampolineScheduler.instance()
}