package aleixbdm.com.techchallenge.repository.event

import aleixbdm.com.techchallenge.*
import aleixbdm.com.techchallenge.core.Result
import aleixbdm.com.techchallenge.database.event.EventDao
import aleixbdm.com.techchallenge.database.event.EventWithImagesEntity
import aleixbdm.com.techchallenge.database.event.toEntity
import aleixbdm.com.techchallenge.repository.RepositoryStatus
import aleixbdm.com.techchallenge.TestSchedulerProvider
import aleixbdm.com.techchallenge.database.event.FavouriteEntity
import aleixbdm.com.techchallenge.webservice.WebServiceInjector
import aleixbdm.com.techchallenge.webservice.event.EventListResponse
import aleixbdm.com.techchallenge.webservice.event.EventResponse
import aleixbdm.com.techchallenge.webservice.event.EventWebService
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import shared.ResultTestData
import shared.fromJson
import shared.liveDataRule

class EventRepositoryTest {

    @get:Rule
    val liveDataRule = liveDataRule()

    private object Module : EventRepositoryModule

    private val converter = WebServiceInjector.converter()

    private val webService: EventWebService = mock()
    private val dao: EventDao = mock()
    private val testable = Module.repository(webService, dao,
        TestSchedulerProvider
    )

    /** Event List LiveData **/

    @Test
    fun eventListLiveData() {
        val json = readJson("eventList.json")
        val response: EventListResponse = converter.fromJson(json)
        val entity = response.embedded.eventList.map { it.toEntity() }
        val data = ResultTestData.success(entity)

        eventListLiveDataTest(
            expected = entity.map { it.toModel() },
            data = data
        )
    }

    @Test
    fun `eventListLiveData when error`() {
        eventListLiveDataTest(
            expected = null,
            data = ResultTestData.error()
        )
    }

    private fun eventListLiveDataTest(
        expected: List<EventModel>?,
        data: ResultTestData<List<EventWithImagesEntity>>
    ) {
        whenever(dao.getEventList()).doAnswer {
            data.toLiveData()
        }

        val actual = testable.eventListLiveData.getValueForTest()
        assertEquals(expected, actual)

        verify(dao).getEventList()
    }

    /** Favourite List LiveData **/

    @Test
    fun favouriteListLiveData() {
        val json = readJson("eventList.json")
        val response: EventListResponse = converter.fromJson(json)
        val entity = response.embedded.eventList.map { it.toEntity() }
        val data = ResultTestData.success(entity)

        favouriteListLiveData(
            expected = entity.map { it.toModel() },
            data = data
        )
    }

    @Test
    fun `favouriteListLiveData when error`() {
        favouriteListLiveData(
            expected = null,
            data = ResultTestData.error()
        )
    }

    private fun favouriteListLiveData(
        expected: List<EventModel>?,
        data: ResultTestData<List<EventWithImagesEntity>>
    ) {
        whenever(dao.getFavouriteList()).doAnswer {
            data.toLiveData()
        }

        val actual = testable.favouriteListLiveData.getValueForTest()
        assertEquals(expected, actual)

        verify(dao).getFavouriteList()
    }

    /** Get Event List **/

    @Test
    fun getEventList() {
        val json = readJson("eventList.json")
        val response: EventListResponse = converter.fromJson(json)
        val data = ResultTestData.success(response)

        getEventListTest(
            data = data
        )
    }

    @Test
    fun `getEventList when error`() {
        getEventListTest(
            data = ResultTestData.error()
        )
    }

    private fun getEventListTest(data: ResultTestData<EventListResponse>) {
        whenever(webService.getEventList(any())).doAnswer {
            Single.just(data.result)
        }

        val actual = testable.getEventList(testEventRequest).test()

        actual.assertComplete()
        actual.assertValueCount(1)

        verify(webService).getEventList(testEventRequest)

        data.result.evaluate(onSuccess = { value ->
            val expected = value.embedded.eventList.map { it.toEntity() }
            val captor = argumentCaptor<List<EventWithImagesEntity>>()

            verify(dao).insertEventWithImagesList(captor.capture())

            val capturedList = captor.allValues.flatten()

            assertEquals(expected.size, capturedList.size)
            val expectedEvents = expected.map { it.event }
            val actualEvents = capturedList.map { it.event }
            assertTrue(expectedEvents.containsAll(actualEvents))
            val expectedImageList = expected.map { it.imageList }
            val actualImageList = capturedList.map { it.imageList }
            assertTrue(expectedImageList.containsAll(actualImageList))

            actual.assertValues(Result.success(RepositoryStatus.Completed))
        }, onError = {
            actual.assertValues(Result.error(it))
        })

        actual.dispose()
    }

    /** Insert Favourite **/

    @Test
    fun insertFavourite() {
        val json = readJson("event.json")
        val response: EventResponse = converter.fromJson(json)

        insertFavouriteTest(
            eventId = response.id
        )
    }

    private fun insertFavouriteTest(eventId: String) {
        val actual = testable.insertFavourite(eventId).test()

        actual.assertComplete()
        actual.assertValueCount(1)

        val captor = argumentCaptor<FavouriteEntity>()
        verify(dao).insertFavourite(captor.capture())
        assertEquals(1, captor.allValues.size)
        assertEquals(FavouriteEntity(eventId), captor.allValues.first())

        actual.assertValues(Result.success(RepositoryStatus.Completed))

        actual.dispose()
    }

    /** Remove Favourite **/

    @Test
    fun removeFavourite() {
        val json = readJson("event.json")
        val response: EventResponse = converter.fromJson(json)

        removeFavouriteTest(
            eventId = response.id
        )
    }

    private fun removeFavouriteTest(eventId: String) {
        val actual = testable.removeFavourite(eventId).test()

        actual.assertComplete()
        actual.assertValueCount(1)

        val captor = argumentCaptor<FavouriteEntity>()
        verify(dao).removeFavourite(captor.capture())
        assertEquals(1, captor.allValues.size)
        assertEquals(FavouriteEntity(eventId), captor.allValues.first())

        actual.assertValues(Result.success(RepositoryStatus.Completed))

        actual.dispose()
    }
}