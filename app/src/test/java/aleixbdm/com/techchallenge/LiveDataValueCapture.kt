package aleixbdm.com.techchallenge

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.junit.Assert.assertEquals

private const val CAPTURE_TIMEOUT = 2000L

class LiveDataValueCapture<T> {

    private val _values = mutableListOf<T?>()
    val values: List<T?>
        get() = _values

    private val channel = Channel<T?>(Channel.UNLIMITED)

    fun addValue(value: T?) {
        _values += value
        channel.offer(value)
    }

    suspend fun assertChanges(times: Int) {
        if (values.size == times) {
            return
        }
        try {
            withTimeout(CAPTURE_TIMEOUT) {
                for (value in channel) {
                    if (values.size == times) {
                        return@withTimeout
                    }
                }
            }
        } catch (ex: TimeoutCancellationException) {
            assertEquals(values.size, times)
        }
    }
}

inline fun <T> LiveData<T>.captureValues(
    times: Int,
    emitterBlock: () -> Unit
): LiveDataValueCapture<T> {
    val capture = LiveDataValueCapture<T>()
    val observer = Observer<T> {
        capture.addValue(it)
    }
    observeForever(observer)
    emitterBlock()
    runBlocking {
        capture.assertChanges(times)
    }
    removeObserver(observer)
    return capture
}

fun <T> LiveData<T>.getValueForTest(): T? {
    var value: T? = null
    val observer = Observer<T> {
        value = it
    }
    observeForever(observer)
    removeObserver(observer)
    return value
}