package aleixbdm.com.techchallenge.viewmodel.event

import aleixbdm.com.techchallenge.*
import aleixbdm.com.techchallenge.core.Result
import aleixbdm.com.techchallenge.database.event.toEntity
import aleixbdm.com.techchallenge.repository.RepositoryStatus
import aleixbdm.com.techchallenge.repository.event.EventModel
import aleixbdm.com.techchallenge.repository.event.EventRepository
import aleixbdm.com.techchallenge.repository.event.toModel
import aleixbdm.com.techchallenge.viewmodel.ViewModelStatus
import aleixbdm.com.techchallenge.webservice.WebServiceInjector
import aleixbdm.com.techchallenge.webservice.event.EventListResponse
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import shared.ResultTestData
import shared.fromJson
import shared.liveDataRule

class EventViewModelTest {

    @get:Rule
    val liveDataRule = liveDataRule()

    private val converter = WebServiceInjector.converter()

    private val repository: EventRepository = mock()
    private val resolver = EventViewModelResolver(
        repository,
        TestSchedulerProvider
    )
    private val testable =
        EventViewModelModule.factory(resolver).create(EventViewModel::class.java)

    /** Event List LiveData **/

    @Test
    fun eventListLiveData() {
        val json = readJson("eventList.json")
        val response: EventListResponse = converter.fromJson(json)
        val model = response.embedded.eventList.map { it.toEntity() }.map { it.toModel() }
        val data = ResultTestData.success(model)

        eventListLiveDataTest(
            expected = model.map { it.toViewData() },
            data = data
        )
    }

    @Test
    fun `eventListLiveData when error`() {
        eventListLiveDataTest(
            expected = null,
            data = ResultTestData.error()
        )
    }

    private fun eventListLiveDataTest(
        expected: List<EventViewData>?,
        data: ResultTestData<List<EventModel>>
    ) {
        whenever(repository.eventListLiveData).doAnswer {
            data.toLiveData()
        }

        val actual = testable.eventListLiveData.getValueForTest()
        assertEquals(expected, actual)

        verify(repository).eventListLiveData
    }

    /** Favourite List LiveData **/

    @Test
    fun favouriteListLiveData() {
        val json = readJson("eventList.json")
        val response: EventListResponse = converter.fromJson(json)
        val model = response.embedded.eventList.map { it.toEntity() }.map { it.toModel() }
        val data = ResultTestData.success(model)

        favouriteListLiveDataTest(
            expected = model.map { it.toViewData() },
            data = data
        )
    }

    @Test
    fun `favouriteListLiveData when error`() {
        favouriteListLiveDataTest(
            expected = null,
            data = ResultTestData.error()
        )
    }

    private fun favouriteListLiveDataTest(
        expected: List<EventViewData>?,
        data: ResultTestData<List<EventModel>>
    ) {
        whenever(repository.favouriteListLiveData).doAnswer {
            data.toLiveData()
        }

        val actual = testable.favouriteListLiveData.getValueForTest()
        assertEquals(expected, actual)

        verify(repository).favouriteListLiveData
    }

    /** Get Event List **/

    @Test
    fun getEventList() {
        val status = RepositoryStatus.Completed
        val data = ResultTestData.success(status)

        getEventListTest(
            data = data
        )
    }

    @Test
    fun `getEventList when error`() {
        getEventListTest(
            data = ResultTestData.error()
        )
    }

    private fun getEventListTest(data: ResultTestData<RepositoryStatus>) {
        whenever(repository.getEventList(any())).doAnswer {
            Single.just(data.result)
        }

        val actual = testable.getEventListStatus.captureValues(2) {
            testable.getEventList()
        }
        val capturedValues = actual.values

        verify(repository).getEventList(any())

        data.result.evaluate(onSuccess = {
            val expected = listOf(
                Result.success(ViewModelStatus.Loading),
                Result.success(ViewModelStatus.Completed)
            )
            assertEquals(expected, capturedValues)

        }, onError = {
            val expected = listOf(
                Result.success(ViewModelStatus.Loading),
                Result.error(it)
            )
            assertEquals(expected, capturedValues)
        })
    }

    /** Insert Favourite **/

    @Test
    fun insertFavourite() {
        val status = RepositoryStatus.Completed
        val data = ResultTestData.success(status)

        insertFavouriteTest(
            data = data
        )
    }

    @Test
    fun `insertFavourite when error`() {
        insertFavouriteTest(
            data = ResultTestData.error()
        )
    }

    private fun insertFavouriteTest(data: ResultTestData<RepositoryStatus>) {
        whenever(repository.insertFavourite(testEventId)).doAnswer {
            Single.just(data.result)
        }

        val actual = testable.insertFavouriteStatus.captureValues(2) {
            testable.insertFavourite(testEventId)
        }
        val capturedValues = actual.values

        verify(repository).insertFavourite(testEventId)

        data.result.evaluate(onSuccess = {
            val expected = listOf(
                Result.success(ViewModelStatus.Loading),
                Result.success(ViewModelStatus.Completed)
            )
            assertEquals(expected, capturedValues)

        }, onError = {
            val expected = listOf(
                Result.success(ViewModelStatus.Loading),
                Result.error(it)
            )
            assertEquals(expected, capturedValues)
        })
    }

    /** Remove Favourite **/

    @Test
    fun removeFavourite() {
        val status = RepositoryStatus.Completed
        val data = ResultTestData.success(status)

        removeFavouriteTest(
            data = data
        )
    }

    @Test
    fun `removeFavourite when error`() {
        removeFavouriteTest(
            data = ResultTestData.error()
        )
    }

    private fun removeFavouriteTest(data: ResultTestData<RepositoryStatus>) {
        whenever(repository.removeFavourite(testEventId)).doAnswer {
            Single.just(data.result)
        }

        val actual = testable.removeFavouriteStatus.captureValues(2) {
            testable.removeFavourite(testEventId)
        }
        val capturedValues = actual.values

        verify(repository).removeFavourite(testEventId)

        data.result.evaluate(onSuccess = {
            val expected = listOf(
                Result.success(ViewModelStatus.Loading),
                Result.success(ViewModelStatus.Completed)
            )
            assertEquals(expected, capturedValues)

        }, onError = {
            val expected = listOf(
                Result.success(ViewModelStatus.Loading),
                Result.error(it)
            )
            assertEquals(expected, capturedValues)
        })
    }
}