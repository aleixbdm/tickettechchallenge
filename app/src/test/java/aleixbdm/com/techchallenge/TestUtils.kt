package aleixbdm.com.techchallenge

import aleixbdm.com.techchallenge.webservice.event.EventRequest
import shared.FailTestException
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.util.*

fun <T : Any> T.readJson(path: String): String {
    val file = javaClass.classLoader?.getResourceAsStream("assets/$path")
        ?: throw FailTestException("No file found on $path")
    return String(file.readBytes())
}

fun date(text: String): Date =
    Date.from(Instant.from(DateTimeFormatter.ISO_INSTANT.parse(text)))

val testEventRequest = EventRequest(
    apiKey = "TEST_API_KEY"
)

const val testEventId = "TEST_ID"